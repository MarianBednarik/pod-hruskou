module.exports = {
  "extends": "airbnb",
  "settings": {
    "import/resolver": {
      "node": {
        "paths": "src"
      }
    }
  },
  "rules": {
    "import/no-extraneous-dependencies": "off",
    "import/no-unresolved": "off",
    "import/extensions": "off",
  }
};