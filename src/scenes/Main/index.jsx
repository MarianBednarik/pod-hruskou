import { connect } from 'react-redux';
import { Switch, Route, withRouter } from 'react-router-dom';
import fetchMeals from 'services/actions';
import React, { Component } from 'react';

import Footer from './components/Footer';
import MenuBar from './components/MenuBar';
import SceneAbout from './scenes/About';
import SceneAlbum from './scenes/Album';
import SceneContact from './scenes/Contact';
import SceneHome from './scenes/Home';
import SceneMenu from './scenes/Menu';

class Main extends Component {
  componentDidMount() {
    this.props.fetchMealsLocal();
  }

  render() {
    return (
      <div>
        <MenuBar />
        <Switch>
          <Route path="/menu" component={SceneMenu} />
          <Route path="/about" component={SceneAbout} />
          <Route path="/album" component={SceneAlbum} />
          <Route path="/contact" component={SceneContact} />
          <Route path="/" component={SceneHome} />
        </Switch>
        <Footer />
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  fetchMealsLocal: () => dispatch(fetchMeals()),
});

export default withRouter(connect(null, mapDispatchToProps)(Main));
