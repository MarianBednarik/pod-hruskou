import React, { Component, Fragment } from 'react';
import styled from 'styled-components';
import FluidContainer from 'components/FluidContainer';
import _Brand from 'components/Brand';
import _Menu from 'scenes/Main/components/MenuBar/components/Menu';
import IconButton from 'components/IconButton';

const HeaderContainer = styled(FluidContainer) `
  width: 100%;
  background: #3E2723;
  height: fit-content;
  display: flex;
  box-shadow: 0 5px 16px rgba(0,0,0,0.5);
  flex-direction: row;
`;

const MenuToggle = styled(IconButton) `
  display: flex;
  align-self: center;
  @media(min-width:576px){
    display: none;
  }
`;

const MobileMenu = styled(_Menu) `
  background-color: #3E2723;
  padding-bottom: 1rem;
`;

const Menu = styled(_Menu) `
  display: none;
  @media(min-width:576px){
    display: flex;
  }
`;

const Brand = styled(_Brand) `
  margin-right: auto;
`;

export default class MenuBar extends Component {

  state = {
    isMenuOpened: false,
  }

  toggleMenu() {
    this.setState(previousState => ({
      isMenuOpened: !previousState.isMenuOpened,
    }))
  }

  render() {
    return (
      <Fragment>
        <HeaderContainer className={this.props.className}>
          <Brand src="./logo.png" name="Pod Hruškou" />
          <Menu type="row" style={{paddingBottom: '1rem'}}/>
          <MenuToggle icon="bars" size="lg" onClick={this.toggleMenu.bind(this)} />
        </HeaderContainer>
        { this.state.isMenuOpened && <MobileMenu type="column" onClick={this.toggleMenu.bind(this)}/>}
      </Fragment>
    );
  }
}
