import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  flex-direction: ${props => props.type};
  align-items: center;
  text-transform: uppercase;
  a {
    text-decoration: none;
  }
`;

const MenuItem = styled(Link)`
  color: #fafafa;
  padding: 1rem 1rem 0 0;
  cursor: pointer;
`;


export default ({
  style, className, onClick, type,
}) => (
  <Container className={className} style={style} onClick={onClick} type={type}>
    <MenuItem to="/menu"> Menu </MenuItem>
    <MenuItem to="/about"> O nás </MenuItem>
    <MenuItem to="/album"> Album </MenuItem>
    <MenuItem to="/contact"> Kontakt </MenuItem>
  </Container>
);
