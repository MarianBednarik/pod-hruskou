import React from 'react';
import styled from 'styled-components';
import FluidContainer from 'components/FluidContainer';
import Social from './components/Social';

const FooterContainer = styled(FluidContainer)`
  position: absolute;
  bottom: 0;
  width: 100%;
  background-color: #3E2723;
  height: 130px;
  text-align: center;
  color: #795548;
  padding: 1rem 0;
  h3 {
    margin: 0;
  }
  p {
    margin: 0.5rem 0;
  }
`;

export default () => (
  <FooterContainer>
    <h3>Pohostinstvo Pod Hruškou</h3>
    <p>Braväcovo 153, 976 64 Slovensko</p>
    <p>+421 000 000 000</p>
    <Social />
  </FooterContainer>
);
