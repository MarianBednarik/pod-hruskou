import React from 'react';
import Icon from 'react-fontawesome';
import styled from 'styled-components';

const Container = styled.div`
  display: inline;
  span {
    color: inherit;
    padding: 0 8px;
  }
`;

export default () => (
  <Container>
    <Icon name="facebook" size="lg" />
    <Icon name="instagram" size="lg" />
    <Icon name="google" size="lg" />
  </Container>
);
