import React, { Component } from 'react';

import MainHeader from './MainHeader';

export default class Home extends Component {
  render() {
    return (
      <MainHeader />
    );
  }
}
