import React, { Component } from 'react';
import styled from 'styled-components';
import image from './image.jpg';

import FluidContainer from 'components/FluidContainer';
import Container from 'components/Container';


const BaseContainer = styled(FluidContainer)`
  height: calc(100vh - 64px);
  padding: 0;
  background-color: #ccc;
  background-image: url(${image});
  background-repeat: no-repeat;
  background-size: cover;
  background-position-y: center;
  position: relative;
  &::after {
    content: "";
    position: absolute;
    bottom: 0;
    width: inherit;
    height: 40vh;
    background: linear-gradient(
      to top,
      hsl(0, 0%, 0%) 0%,
      hsla(0, 0%, 0%, 0.738) 19%,
      hsla(0, 0%, 0%, 0.541) 34%,
      hsla(0, 0%, 0%, 0.382) 47%,
      hsla(0, 0%, 0%, 0.278) 56.5%,
      hsla(0, 0%, 0%, 0.194) 65%,
      hsla(0, 0%, 0%, 0.126) 73%,
      hsla(0, 0%, 0%, 0.075) 80.2%,
      hsla(0, 0%, 0%, 0.042) 86.1%,
      hsla(0, 0%, 0%, 0.021) 91%,
      hsla(0, 0%, 0%, 0.008) 95.2%,
      hsla(0, 0%, 0%, 0.002) 98.2%,
      hsla(0, 0%, 0%, 0) 100%
    );
  }
  &::before {
    content: "";
    position: absolute;
    top: 0;
    width: inherit;
    height: 32px;
    background: linear-gradient(
      to bottom,
      hsla(0, 0%, 0%, 0.75) 0%,
      hsla(0, 0%, 0%, 0.553) 19%,
      hsla(0, 0%, 0%, 0.405) 34%,
      hsla(0, 0%, 0%, 0.286) 47%,
      hsla(0, 0%, 0%, 0.208) 56.5%,
      hsla(0, 0%, 0%, 0.145) 65%,
      hsla(0, 0%, 0%, 0.094) 73%,
      hsla(0, 0%, 0%, 0.056) 80.2%,
      hsla(0, 0%, 0%, 0.031) 86.1%,
      hsla(0, 0%, 0%, 0.015) 91%,
      hsla(0, 0%, 0%, 0.006) 95.2%,
      hsla(0, 0%, 0%, 0.001) 98.2%,
      hsla(0, 0%, 0%, 0) 100%
    );
  }
`;

const TextContainer = styled(Container)`
  position: absolute;
  bottom: 5rem;
  left: 0;
  z-index: 1;
  @media(min-width:576px){
    left: 2rem;
  }
  @media(min-width:768px){
    left: 3rem;
  }

  @media(min-width:992px){
    left: 5rem;
  }

  @media(min-width:1200px){
    left: 10em;
  } 
`;
const HeaderText = styled.h1`
  color: #fff;
  margin: 0;
  font-size: 2rem;
  text-shadow: 0 0 8px rgba(0,0,0,0.5);
  @media(min-width:576px){
    font-size: 2.9rem;
  }

  @media(min-width:768px){
    font-size: 4rem;
  }
`;

const SubHeaderText = styled.p`
  color: #fff;
  margin: 0;
  font-size: 1.25rem;
  @media(min-width:576px){
    font-size: 2rem;
  }
`;

export default class MainHeader extends Component {
  render() {
    return (
      <BaseContainer>
        <TextContainer>
          <HeaderText>
            Pohostinstvo Pod Hruškou
          </HeaderText>
          <SubHeaderText>
            Najlepšie pohostinstvo v okolí Braväcova
          </SubHeaderText>
        </TextContainer>
      </BaseContainer>
    );
  }
}
