import { connect } from 'react-redux';
import Container from 'components/Container';
import Header from 'components/Header';
import Loading from 'components/Loading';
import React, { Component } from 'react';

import Menu from './components/Menu';

class SceneMenu extends Component {
  render() {
    return (
      <Container>
        <Header>
          Jedálny Lístok
        </Header>
        {this.props.fetching ? <Loading /> : <Menu data={this.props.meals} />}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  fetching: state.fetching,
  fetched: state.fetched,
  error: state.error,
  meals: state.meals,
});

export default connect(mapStateToProps, null)(SceneMenu);
