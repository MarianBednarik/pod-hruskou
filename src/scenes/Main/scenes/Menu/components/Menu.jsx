import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Card from 'components/Card';

const Table = styled.table`
  width: 100%;
  font-size: 1.25rem;
  text-align: left;
`;

const SubText = styled.span`
  font-size: 0.9rem;
  color: #2c2c2c;
  display: block;
`;


export default ({ data }) => {
  const meals = data.map(meal => (
    <tr key={meal.id}>
      <td>{meal.name}<SubText>{meal.ingredients.join(', ')}</SubText></td>
      <td>{meal.weight}g</td>
      <td>{meal.price}€</td>
    </tr>
  ));

  return (
    <Card >
      <Table>
        <tbody>
          <tr>
            <th>Názov</th>
            <th>Váha</th>
            <th>Cena</th>
          </tr>
          {meals}
        </tbody>
      </Table>
    </Card>);
};

