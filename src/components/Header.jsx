import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const MainText = styled.h1`
  color: #3E2723;
  font-size: 2rem;
  position: relative;
  width: fit-content;
  padding-bottom: 1rem;
  &::after {
    content: '';
    position: absolute;
    height: 3px;
    width: 50%;
    bottom: 0px;
    left: 0;
    background-color: #3E2723;
  }
  @media(min-width:576px){
    font-size: 3rem;
  }
`;
const Result = ({ children }) => (
  <MainText>
    {children}
  </MainText>
);

Result.propTypes = {
  children: PropTypes.string,
};

Result.defaultProps = {
  children: '',
};

export default Result;
