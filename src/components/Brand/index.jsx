import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

const ContainerBrand = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  width: fit-content;
  height: 100%;
  text-decoration: none;
  padding: 0.5rem 0;
`;

const Logo = styled.img`
  height: 40px;
  width: 40px;
  margin-right: 1em;
`;

const Name = styled.h1`
  margin: 0;
  color: #fafafa;
  font-family: 'Dancing Script', cursive;
  text-decoration: none;
`;

const Brand = ({ name, className }) => (
  <Link to="/" style={{ textDecoration: 'none' }} className={className}>
    <ContainerBrand>
      <Logo src={require('./logo.png')} />
      <Name>{name}</Name>
    </ContainerBrand>
  </Link>
);

Brand.propTypes = {
  name: PropTypes.string,
};

Brand.defaultProps = {
  name: 'PLACEHOLDER',
};

export default Brand;

