import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  width: 70px;
  text-align: center;
`;

const Dot = styled.div`
  width: 16px;
  height: 16px;
  margin: 2px;
  background-color: #3E2723;
  border-radius: 100%;
  display: inline-block;
  -webkit-animation: sk-bouncedelay 1.4s infinite ease-in-out both;
  -webkit-animation-delay: ${props => props.delay}s;
  animation: sk-bouncedelay 1.4s infinite ease-in-out both;
  animation-delay: ${props => props.delay}s;

  @-webkit-keyframes sk-bouncedelay {
    0%, 80%, 100% { -webkit-transform: scale(0) }
    40% { -webkit-transform: scale(1.0) }
  }

  @keyframes sk-bouncedelay {
    0%, 80%, 100% { 
      -webkit-transform: scale(0);
      transform: scale(0);
    } 40% { 
      -webkit-transform: scale(1.0);
      transform: scale(1.0);
    }
  } 
`;

export default () => (
  <Container>
    <Dot delay={0} />
    <Dot delay={0.16} />
    <Dot delay={0.32} />
  </Container>
);
