import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const ButtonBase = styled.button`
  background-color: #aaa;
`;

const Button = ({ children }) => (
  <ButtonBase> {children.toUpperCase()} </ButtonBase>
);

Button.propTypes = {
  children: PropTypes.string,
};

Button.defaultProps = {
  children: 'Button',
};

export default Button;
