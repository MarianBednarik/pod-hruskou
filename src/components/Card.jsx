import React from 'react';
import styled from 'styled-components';

const Card = styled.div`
  width: 100%;
  height: fit-content;
  background-color: #fafafa;
  border-radius: 6px;
  box-shadow: 2px 2px 10px rgba(0,0,0,0.25);
  padding: 1rem 1.25rem;
`;

export default ({ children }) => (
  <Card>
    {children}
  </Card>
);
