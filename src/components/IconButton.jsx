import React from 'react';
import Icon from 'react-fontawesome';
import styled from 'styled-components';

const Button = styled.a`
  color: #fafafa;
  cursor: pointer;
`;

export default ({
  icon, size, onClick, className,
}) => (
  <Button className={className} onClick={onClick}>
    <Icon name={icon} size={size} />
  </Button>
);
