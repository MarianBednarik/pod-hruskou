import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const ContainerBase = styled.div`
  width: 100%;
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
`;

const FluidContainer = ({ children, className }) => (
  <ContainerBase className={className}> {children} </ContainerBase>
);

FluidContainer.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
};

FluidContainer.defaultProps = {
  children: null,
  className: '',
};

export default FluidContainer;
