const reducer = (state, action) => {
  switch (action.type) {
    case 'FETCH_MEALS_PENDING': {
      return { ...state, fetching: true };
    }
    case 'FETCH_MEALS_FULFILLED': {
      return {
        ...state,
        fetching: false,
        fetched: true,
        meals: action.payload,
      };
    }
    case 'FETCH_MEALS_REJECTED': {
      return {
        ...state,
        fetching: false,
        error: action.payload,
      };
    }
    default: {
      return state;
    }
  }
};

export default reducer;

