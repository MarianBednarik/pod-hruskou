import { firestore } from './firebase';

const fetchMeals = () => {
  const meals = [];
  return (dispatch) => {
    dispatch({ type: 'FETCH_MEALS_PENDING' });
    firestore.collection('food')
      .get()
      .then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          const meal = doc.data();
          meal.id = doc.id;
          meals.push(meal);
        });
        dispatch({ type: 'FETCH_MEALS_FULFILLED', payload: meals });
      }).catch((error) => {
        dispatch({ type: 'FETCH_MEALS_REJECTED', payload: error });
      });
  };
};

export default fetchMeals;
