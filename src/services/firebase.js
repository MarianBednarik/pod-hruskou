import firebase from 'firebase';

require('firebase/firestore');

const config = undefined;

firebase.initializeApp(config);
export const firestore = firebase.firestore();
export default firebase;

