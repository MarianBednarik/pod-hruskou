import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import MainScene from 'scenes/Main';
import React from 'react';
import ReactDOM from 'react-dom';
import store from 'services/store.js';

import registerServiceWorker from './registerServiceWorker';

import './index.css';

ReactDOM.render(<Provider store={store}><Router><MainScene /></Router></Provider>, document.getElementById('root'));
registerServiceWorker();
